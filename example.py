from paf_utils import Paf

## FTP url for H. stenplepic RefSeq assembly
url_histen = "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/013/339/905/GCF_013339905.1_IPHC_HiSten_1.0"

## Example using H. hippoglossus
spp = "H. hippoglossus"
url = "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/009/819/705/GCF_009819705.1_fHipHip1.pri"
mapping = "example-files/map_GCF_009819705.1_fHipHip1.pri_genomic_to_GCF_013339905.1_IPHC_HiSten_1.0_genomic.paf"


paf = Paf(
    paf_file=mapping,
    spp_trgt="H. stenolepis",
    url_trgt=url_histen,
    spp_qry=spp,
    url_qry=url,
)
paf.process_paf() ## must call before plotting

paf.plot_karyotype() ## make karyotype plot
paf.plot_coverage(show_plot=True) ## query coverage 
paf.plot_coverage(query_coverage=False, show_plot=True) ## target coverage


## Example using C. semilaevis
spp = "C. semilaevis"
url = (
    "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/523/025/GCF_000523025.1_Cse_v1.0"
)
mapping = "example-files/map_GCF_000523025.1_Cse_v1.0_genomic_to_GCF_013339905.1_IPHC_HiSten_1.0_genomic.paf"

paf = Paf(
    paf_file=mapping,
    spp_trgt="H. stenolepis",
    url_trgt=url_histen,
    spp_qry=spp,
    url_qry=url,
)
paf.process_paf()

paf.plot_karyotype()
paf.plot_coverage(show_plot=True)
paf.plot_coverage(query_coverage=False, show_plot=True)
