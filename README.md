
```python
from paf_utils import Paf
```

FTP url for H. stenplepis RefSeq assembly.  Used for retrieval of the annotatation report directly from NCBI.
```python
url_histen = "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/013/339/905/GCF_013339905.1_IPHC_HiSten_1.0"
```

Example using H. hippoglossus.
```python
spp = "H. hippoglossus"
url = "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/009/819/705/GCF_009819705.1_fHipHip1.pri"
mapping = "example-files/map_GCF_009819705.1_fHipHip1.pri_genomic_to_GCF_013339905.1_IPHC_HiSten_1.0_genomic.paf"

paf = Paf(
    paf_file=mapping,
    spp_trgt="H. stenolepis",
    url_trgt=url_histen,
    spp_qry=spp,
    url_qry=url,
)
```
___IMPORTANT:___  Process_paf() must be called before any plotting is done.  Could probably put into ```__init__``` in order to save a step.
```python
paf.process_paf() ## must call before plotting
```
Make a karyotype plot from PAF file.  Modeled after the karyotype plots implemented in jcvi (https://github.com/tanghaibao/jcvi/wiki/MCscan-%28Python-version%29#macrosynteny-visualization) for macrosynteny visualization.
```python
paf.plot_karyotype() ## make karyotype plot
```
Coverage plots.  Modeled after the coverage plots implemented in the pafr R package (https://cran.r-project.org/web/packages/pafr/index.html).  Plot coverage of the query genome by the target genome.
```python
paf.plot_coverage(show_plot=True) ## query coverage 
```
Plot coverage of the target genome by the query genome.
```python
paf.plot_coverage(query_coverage=False, show_plot=True) ## target coverage
```

Same example as above but using C. semilaevis
```python
spp = "C. semilaevis"
url = (
    "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/523/025/GCF_000523025.1_Cse_v1.0"
)
mapping = "example-files/map_GCF_000523025.1_Cse_v1.0_genomic_to_GCF_013339905.1_IPHC_HiSten_1.0_genomic.paf"

paf = Paf(
    paf_file=mapping,
    spp_trgt="H. stenolepis",
    url_trgt=url_histen,
    spp_qry=spp,
    url_qry=url,
)
paf.process_paf() ## must call before plotting

paf.plot_karyotype()
paf.plot_coverage(show_plot=True)
paf.plot_coverage(query_coverage=False, show_plot=True)
```