from multiprocessing.dummy import active_children
from re import M
import pandas as pd
import numpy as np

import urllib

from natsort import natsorted, natsort_keygen

import bezier

import matplotlib.pyplot as plt
from matplotlib.path import Path as mpath
from matplotlib.patches import PathPatch
import matplotlib.patheffects as pe
import matplotlib.colors as mplcolors

from bokeh.plotting import figure, show, save, output_file
from bokeh.models import ColumnDataSource, FuncTickFormatter, Legend

from tabulate import tabulate
import colorcet as cc

from . import helpers as helpers

plt.style.use("fast")


class Paf:
    """A class for processing and plotting PAF files produced by D-GENIES

    Args:
        paf_file (str): Path to paf file from D-GENIES.
        spp_qry (str): Species name of genome used as the query sequence.
        url_qry (str): For the FTP directory for RefSeq or GenBank assembly for the query genome.
        spp_trgt (str): Species name of genome used as the tharget sequence.
        url_trgt (str): Url for the FTP directory for RefSeq or GenBank assembly for the target genome.
    """    

    def __init__(
        self, paf_file: str, spp_qry: str, url_qry: str, spp_trgt: str, url_trgt: str
    ):
       

        self.paf_file = paf_file
        self.url_qry = url_qry
        self.spp_qry = spp_qry
        self.url_trgt = url_trgt
        self.spp_trgt = spp_trgt

    def process_paf(self):
        """
        Process PAF file for futher plotting. Basically it renames chromosomes from NCBI
        acession numbers to the assigned chromosome number.
        NOTE: ***MUST*** be called prior to plotting...this could be easily fixed though.
        """
        syn_df = helpers.paf2pandas(self.paf_file)

        ## Retrieve annotation report from NCBI to determine which chrs to plot
        if self.url_qry is not None:
            print(f"QRY URL IS {self.url_qry}")
            self.rpt_qry = self.get_assembly_report(self.url_qry)
            if self.rpt_qry.iloc[0]["RefSeq-Accn"] == "na":
                self.rpt_qry = self.rpt_qry.set_index("GenBank-Accn")
            else:
                self.rpt_qry = self.rpt_qry.set_index("RefSeq-Accn")

            self.rpt_qry["plot_name"] = self.rpt_qry.apply(
                lambda x: f"{x['Assigned-Molecule']}"
                if x["Assigned-Molecule-Location/Type"]
                in ["Chromosome", "Linkage Group"]
                else x.name,
                axis=1,
            )
            self.seq_dict_q = self.rpt_qry.to_dict("index")
            map_qry = {
                row.name: f"{row['Assigned-Molecule']}"
                if row["Assigned-Molecule-Location/Type"]
                in ["Chromosome", "Linkage Group"]
                else "unplaced"
                for ix, row in self.rpt_qry.iterrows()
            }
        else:
            self.seq_dict_q, map_qry = helpers.make_asembly_report(syn_df, query=True)

        if self.url_trgt is not None:
            print(f"QRY URL IS {self.url_trgt}")
            self.rpt_trgt = self.get_assembly_report(self.url_trgt)
            if self.rpt_trgt.iloc[0]["RefSeq-Accn"] == "na":
                self.rpt_trgt = self.rpt_trgt.set_index("GenBank-Accn")
            else:
                self.rpt_trgt = self.rpt_trgt.set_index("RefSeq-Accn")

            self.rpt_trgt["plot_name"] = self.rpt_trgt.apply(
                lambda x: f"{x['Assigned-Molecule']}"
                if x["Assigned-Molecule-Location/Type"]
                in ["Chromosome", "Linkage Group"]
                else x.name,
                axis=1,
            )
            self.seq_dict_t = self.rpt_trgt.to_dict("index")
            map_trgt = {
                row.name: f"Chr_{row['Assigned-Molecule']}"
                if row["Assigned-Molecule-Location/Type"]
                in ["Chromosome", "Linkage Group"]
                else "unplaced"
                for ix, row in self.rpt_trgt.iterrows()
            }
        else:
            self.seq_dict_t, map_trgt = helpers.make_asembly_report(syn_df, query=False)

        self.chr_map = {
            **map_qry,
            **map_trgt,
        }

        syn_df["q_chrom"] = syn_df["Query sequence name"].replace(self.chr_map)
        syn_df["t_chrom"] = syn_df["Target sequence name"].replace(self.chr_map)

        syn_df["identity"] = (
            syn_df["Number of residue matches"] / syn_df["Alignment block length"]
        )
        syn_df["query_cov"] = (syn_df["Query end"] - syn_df["Query start"]) / syn_df[
            "Query sequence length"
        ]

        syn_df["target_cov"] = (
            syn_df["Target end on original strand"]
            - syn_df["Target start on original strand"]
        ) / syn_df["Target sequence length"]

        syn_df = syn_df.sort_values("identity", ascending=False)

        ## -------------------------------------------------------------------------------------
        ## Check CHROM lengths and update the sequence dict
        ## NOTE THIS IS ONLY AN ACCOMADTION FOR IPHC_HiStenV2
        ## SHOULD NOT BE DONE NORMALLY!
        for ix, row in (
            syn_df[["Query sequence name", "Query sequence length"]]
            .drop_duplicates()
            .sort_values("Query sequence name")
            .iterrows()
        ):
            if row["Query sequence name"] in self.seq_dict_q.keys():
                if int(
                    self.seq_dict_q[row["Query sequence name"]]["Sequence-Length"]
                ) != int(row["Query sequence length"]):
                    print(
                        f'BE WARNED: Sequence lengths for the query seuqneces {row["Query sequence name"]} do not match in the PAF file and sequence dictionary obtained from NCBI.  Overriding lengths from NCBI with those in the PAF file'
                    )
                    self.seq_dict_q[row["Query sequence name"]][
                        "Sequence-Length"
                    ] = int(row["Query sequence length"])
            else:
                self.seq_dict_q[row["Query sequence name"]] = {
                    "Sequence-Length": int(row["Query sequence length"]),
                    "plot_name": row["Query sequence name"],
                }

        for ix, row in (
            syn_df[["Target sequence name", "Target sequence length"]]
            .drop_duplicates()
            .sort_values("Target sequence name")
            .iterrows()
        ):
            if row["Target sequence name"] in self.seq_dict_t.keys():
                if int(
                    self.seq_dict_t[row["Target sequence name"]]["Sequence-Length"]
                ) != int(row["Target sequence length"]):
                    print(
                        f'BE WARNED: Sequence lengths for the target seuqneces {row["Target sequence name"]} do not match in the PAF file and sequence dictionary obtained from NCBI.  Overriding lengths from NCBI with those in the PAF file'
                    )
                    self.seq_dict_t[row["Target sequence name"]][
                        "Sequence-Length"
                    ] = int(row["Target sequence length"])
            else:
                self.seq_dict_t[row["Target sequence name"]] = {
                    "Sequence-Length": int(row["Target sequence length"]),
                    "plot_name": row["Target sequence name"],
                }

        ## -------------------------------------------------------------------------------------

        print(f"TOTAL ALIGNMENTS: {syn_df.shape[0]:,d}")

        ## Primary only
        mask = syn_df["alignment_tags"].apply(lambda x: x["tp"].split(":")[1] == "P")
        self.secondary_df = syn_df.loc[~mask]
        self.syn_df = syn_df.loc[mask]

        print(
            f"PRIMARY ALIGNMENTS: {self.syn_df.shape[0]:,d}, SECONDARY: {self.secondary_df.shape[0]:,d}"
        )

        ## Assembled Chroms Only
        mask = (
            ~self.syn_df["t_chrom"].str.startswith("unplaced")
            & ~self.syn_df["t_chrom"].str.startswith("grouped")
            & ~self.syn_df["t_chrom"].str.startswith("HiC")
        ) & (
            ~self.syn_df["q_chrom"].str.startswith("unplaced")
            & ~self.syn_df["q_chrom"].str.startswith("grouped")
            & ~self.syn_df["q_chrom"].str.startswith("HiC")
        )

        self.syn_df_unplaced = self.syn_df.loc[~mask]
        self.syn_df = self.syn_df.loc[mask]

        print(
            f"ASSEMBLED CHROMOSOME ALIGNMENTS: {self.syn_df.shape[0]:,d} ({self.syn_df_unplaced.shape[0]:,d} in unplaced)"
        )

    def get_assembly_report(self, url: str):
        """Retrives basic info from NCBI for a given genome assembly.

        Args:
            url (str): url to RefSeq or GenBank FTP direcory for genome assembly.

        Returns:
            _type_: _description_
        """

        file = urllib.request.urlopen(f'{url}/{url.split("/")[-1]}_assembly_report.txt')
        lines = [line.decode().strip().split("\t") for line in file]

        keep = False
        data = []
        for i in lines:
            if i[0] == "# Sequence-Name":
                keep = True
            if keep:
                data.append(i)

        annotation_rpt = pd.DataFrame(data[1:], columns=data[0])
        return annotation_rpt

    def filter_hits(self, rng: tuple, query: bool = False):
        """Filter hits in PAF file to a specific region

        Args:
            rng (tuple): the range to restrict hits to use the form (CHROM, START, STOP)
            query (bool, optional): filter based on query or target (default) coordinates. Defaults to False.

        Returns:
            pd.DataFrame: Dataframe containing hits within a specific region of of the query or reference.
        """

        syn_df = pd.concat([self.syn_df, self.syn_df_unplaced, self.secondary_df])
        if query:
            out = syn_df.loc[
                (syn_df["Query sequence name"] == rng[0])
                & (
                    (
                        (rng[1] <= syn_df["Query start"])
                        & (syn_df["Query start"] <= rng[2])
                    )
                    | (
                        (rng[1] <= syn_df["Query end"])
                        & (syn_df["Query end"] <= rng[2])
                    )
                )
            ]
        else:
            out = syn_df.loc[
                (syn_df["Target sequence name"] == rng[0])
                & (
                    (
                        (rng[1] <= syn_df["Target start on original strand"])
                        & (syn_df["Target start on original strand"] <= rng[2])
                    )
                    | (
                        (rng[1] <= syn_df["Target end on original strand"])
                        & (syn_df["Target end on original strand"] <= rng[2])
                    )
                )
            ]
        return out

    def plot_karyotype(self, save: bool = False, outfile: str = None):
        """Generaye karyotype plot for D-GENIES PAF file.  Inspired by the karyotype plots implemented in
        jcvi (https://github.com/tanghaibao/jcvi/wiki/MCscan-%28Python-version%29#macrosynteny-visualization)
        for macrosynteny visualization.

        Args:
            save (bool, optional): Save file? Defaults to False.
            outfile (str, optional): Path to save file. Defaults to None.
        """

        syn_df = self.syn_df.copy()

        order_q = (
            pd.DataFrame(
                [
                    (
                        i,
                        int(self.seq_dict_q[i]["Sequence-Length"]),
                        self.seq_dict_q[i]["plot_name"],
                    )
                    for i in set(self.syn_df["Query sequence name"])
                ]
            )
            .sort_values(by=2, key=natsort_keygen())
            .reset_index(drop=True)
        )
        

        order_t = (
            pd.DataFrame(
                [
                    (
                        i,
                        int(self.seq_dict_t[i]["Sequence-Length"]),
                        self.seq_dict_t[i]["plot_name"],
                    )
                    for i in set(self.syn_df["Target sequence name"])
                ]
            )
            .sort_values(by=2, key=natsort_keygen())
            .reset_index(drop=True)
        )

        gap = 1e7 * 2

        order_q["stop"] = order_q[1].cumsum()
        order_q["start"] = np.roll(order_q["stop"], 1)
        order_q.loc[0, "start"] = 0

        for i in range(1, order_q.shape[0]):
            order_q.loc[i:, "start"] = order_q.loc[i:, "start"] + gap
            order_q.loc[i:, "stop"] = order_q.loc[i:, "stop"] + gap

        order_q["y"] = 0

        order_t["stop"] = order_t[1].cumsum()
        order_t["start"] = np.roll(order_t["stop"], 1)
        order_t.loc[0, "start"] = 0

        for i in range(1, order_t.shape[0]):
            order_t.loc[i:, "start"] = order_t.loc[i:, "start"] + gap
            order_t.loc[i:, "stop"] = order_t.loc[i:, "stop"] + gap

        order_t["y"] = 1

        # center around midpoint
        q_mid = order_q["stop"].max() / 2
        t_mid = order_t["stop"].max() / 2

        order_q["start"] = order_q["start"] - q_mid
        order_q["stop"] = order_q["stop"] - q_mid

        order_t["start"] = order_t["start"] - t_mid
        order_t["stop"] = order_t["stop"] - t_mid

        ## set the colors
        syn_df["color"] = "gray"
        syn_df["alpha"] = 0.5

        ### Colorize plots
        sdr = range(14000000, 26000000)
        syn_df.loc[syn_df["Target sequence name"] == "NC_048935.1", "color"] = "#ECD078"
        syn_df.loc[syn_df["Target sequence name"] == "NC_048935.1", "alpha"] = 0.7

        mask_sdr = syn_df[
            ["Target start on original strand", "Target end on original strand"]
        ].apply(lambda x: any([i in sdr for i in x]), axis=1)

        syn_df.loc[
            (syn_df["Target sequence name"] == "NC_048935.1") & (mask_sdr), "color"
        ] = "#D95B43"
        syn_df.loc[
            (syn_df["Target sequence name"] == "NC_048935.1") & (mask_sdr), "alpha"
        ] = 0.7

        ## Categorical with target chr last so that is is plotted on top
        syn_df["Target sequence name"] = pd.Categorical(
            syn_df["Target sequence name"],
            categories=[
                "NC_048927.1",
                "NC_048928.1",
                "NC_048929.1",
                "NC_048930.1",
                "NC_048931.1",
                "NC_048932.1",
                "NC_048933.1",
                "NC_048934.1",
                "NC_048936.1",
                "NC_048937.1",
                "NC_048938.1",
                "NC_048939.1",
                "NC_048940.1",
                "NC_048941.1",
                "NC_048942.1",
                "NC_048943.1",
                "NC_048944.1",
                "NC_048945.1",
                "NC_048946.1",
                "NC_048947.1",
                "NC_048948.1",
                "NC_048949.1",
                "NC_048950.1",
                "NC_048935.1",
            ],
        )

        # plot the data

        fig = plt.figure(figsize=(12, 2.25))
        ax = fig.add_subplot(1, 1, 1)

        for ix, row in order_q.iterrows():
            ax.plot(
                [row["start"], row["stop"]],
                [row["y"], row["y"]],
                "-",
                color="#154B8B",
                path_effects=[pe.Stroke(linewidth=6, foreground="gray"), pe.Normal()],
                lw=4,
                solid_capstyle="round",
            )

        for ix, row in order_t.iterrows():
            ax.plot(
                [row["start"], row["stop"]],
                [row["y"], row["y"]],
                "-",
                color="#FF9A0D",
                path_effects=[pe.Stroke(linewidth=6, foreground="gray"), pe.Normal()],
                lw=4,
                solid_capstyle="round",
            )

        label_props = {"ha": "center", "va": "center", "size": "small"}
        for ix, row in order_q.iterrows():
            text = self.chr_map[row[0]].replace("Chr_", "")
            y = row["y"] - 0.1
            x = np.mean([row["start"], row["stop"]])
            ax.text(x, y, text, **label_props)

        for ix, row in order_t.iterrows():
            text = self.chr_map[row[0]].replace("Chr_", "")
            y = row["y"] + 0.08
            x = np.mean([row["start"], row["stop"]])
            ax.text(x, y, text, **label_props)

        label_props = {"style": "italic", "ha": "right", "va": "center"}
        ax.text(order_q.iloc[0]["start"] - 1e7, 0, self.spp_qry, **label_props)
        ax.text(order_t.iloc[0]["start"] - 1e7, 1, self.spp_trgt, **label_props)

        ## group by target first so categorical sort order is respected
        for grp, df in syn_df.groupby(["Target sequence name", "Query sequence name"]):
            # print(grp)
            shift = order_q.loc[order_q[0] == grp[1], "start"].iloc[0]
            df["Query start"] = df["Query start"] + shift
            df["Query end"] = df["Query end"] + shift

            shift = order_t.loc[order_t[0] == grp[0], "start"].iloc[0]
            df["Target start on original strand"] = (
                df["Target start on original strand"] + shift
            )
            df["Target end on original strand"] = (
                df["Target end on original strand"] + shift
            )

            ## bezier curves for connections
            curve_y_pad = 0.6
            for ix, row in df.iterrows():
                tx = [
                    row["Target start on original strand"],
                    row["Target end on original strand"],
                ]
                ty = [1, 1]
                if row["Relative strand"] == "-":
                    qx = [row["Query end"], row["Query start"]]
                else:
                    qx = [row["Query start"], row["Query end"]]
                qy = [0, 0]

                nodes = [
                    [tx[0], tx[0], qx[0], qx[0]],
                    [ty[0], ty[0] - curve_y_pad, qy[0] + curve_y_pad, qy[0]],
                ]
                curve = bezier.Curve(nodes, degree=3)
                x0, y0 = curve.evaluate_multi(np.linspace(0.0, 1.0, 50))

                nodes = [
                    [qx[1], qx[1], tx[1], tx[1]],
                    [qy[1], qy[1] + curve_y_pad, ty[1] - curve_y_pad, ty[1]],
                ]
                curve = bezier.Curve(nodes, degree=3)
                x1, y1 = curve.evaluate_multi(np.linspace(0.0, 1.0, 50))

                coords = [i for i in zip(x0, y0)] + [i for i in zip(x1, y1)]

                path = mpath(coords)
                patch = PathPatch(
                    path, facecolor=row["color"], edgecolor="none", alpha=row["alpha"]
                )

                ax.add_patch(patch)

        # Hide the right and top spines
        # Move left and bottom spines outward by 10 points
        ax.spines["left"].set_position(("outward", -15))
        ax.spines["top"].set_position(("outward", -10))

        ax.spines["right"].set_visible(False)
        ax.spines["top"].set_visible(False)

        ax.spines["left"].set_visible(False)
        ax.spines["bottom"].set_visible(False)

        ax.set_yticks([])
        ax.set_xticks([])

        plt.tight_layout()
        if save:
            plt.savefig(outfile, dpi=400)
        plt.show()

    def plot_coverage(
        self,
        show_plot: bool = False,
        query_coverage: bool = True,
        outfile: str = None,
    ):
        """Generate interactive plot of genomic coverage for PAF file.

        Args:
            show_plot (bool, optional): Show plot in browser window when complete? Defaults to False.
            query_coverage (bool, optional): Plot query coverage (default) or target coverage? Defaults to True.
            outfile (str, optional): Path to save output file. A defualt name will be generated if one is not supplied. Defaults to None.

        Returns:
            _type_: _description_
        """

        ## set up variables to handle plotting of query or target coverage here
        if query_coverage:
            sort_cols = ["Query sequence name", "Query start", "Query end"]
            chr_cols = ["Query sequence name", "Target sequence name"]
            renamed_chr_cols = ["q_chrom", "t_chrom"]
            seq_dict = self.seq_dict_q
            spps = [self.spp_qry, self.spp_trgt]
            file_desc = "query"
            x_cols = ["Query start", "Query end"]

            TOOLTIPS = [
                ("target", "@t_chrom (@{Target sequence name})"),
                (
                    "span",
                    "@{Target start on original strand}{0,0}-@{Target end on original strand}{0,0}",
                ),
                ("% identity", "@identity"),
            ]

        else:
            print("Plotting coverage of target geneome by query")
            sort_cols = [
                "Target sequence name",
                "Target start on original strand",
                "Target end on original strand",
            ]
            chr_cols = ["Target sequence name", "Query sequence name"]
            renamed_chr_cols = ["t_chrom", "q_chrom"]
            seq_dict = self.seq_dict_t
            spps = [self.spp_trgt, self.spp_qry]
            file_desc = "target"
            x_cols = [
                "Target start on original strand",
                "Target end on original strand",
            ]

            TOOLTIPS = [
                ("query", "@q_chrom (@{Query sequence name})"),
                (
                    "span",
                    "@{Query start}{0,0}-@{Query end}{0,0}",
                ),
                ("% identity", "@identity"),
            ]

        if outfile is None:
            outfile = f"{spps[0]}-{spps[1]}-{file_desc}-coverage.html".replace(" ", "")

        syn_df = (
            pd.concat([self.syn_df_unplaced, self.syn_df])
            .sort_values(sort_cols)
            .reset_index(drop=True)
        )

        sub = syn_df.loc[
            ~syn_df[renamed_chr_cols[0]].str.startswith("unplaced")
            & ~syn_df[renamed_chr_cols[0]].str.startswith("grouped")
        ]

        cmap = cc.cm.glasbey_hv
        colors = [mplcolors.rgb2hex(cmap(i)) for i in range(cmap.N)]

        # generate colormap unplaced are gray
        cmap = {
            chrom: colors[ix]
            if (~chrom.startswith("unplaced") and ~chrom.startswith("grouped"))
            else "black"
            for ix, chrom in enumerate(set(sub[renamed_chr_cols[1]]))
        }
        plot_cols = [
            "Query sequence name",
            "Query sequence length",
            "Query start",
            "Query end",
            "Relative strand",
            "Target sequence name",
            "Target sequence length",
            "Target start on original strand",
            "Target end on original strand",
            "Number of residue matches",
            "identity",
            "q_chrom",
            "t_chrom",
        ]

        xs = []
        ys = []
        for i in set(sub[chr_cols[0]]):
            ys.append([seq_dict[i]["plot_name"]] * 2)
            xs.append([0, int(seq_dict[i]["Sequence-Length"])])

        bg_source = ColumnDataSource(data={"xs": xs, "ys": ys})

        chroms = natsorted(set([i[0] for i in ys]))
        chroms.reverse()

        w = 1000
        h = (len(chroms) * 20) + int(np.ceil(len(chroms) / 10) * 100)

        output_file(
            filename=outfile,
            title=f"{spps[1]}-{spps[0]}-{file_desc}-coverage",
        )

        p = figure(
            width=w,
            height=h,
            y_range=chroms,
            tooltips=TOOLTIPS,
            title=f"{spps[1]} Coverage of {spps[0]} Genome",
        )
        # hover tool off by
        p.toolbar.active_inspect = None

        ## Layout chromosomes
        p.multi_line(
            xs="xs",
            ys="ys",
            source=bg_source,
            color="black",
            line_cap="round",
            line_width=12,
        )
        p.multi_line(
            xs="xs",
            ys="ys",
            source=bg_source,
            color="white",
            line_cap="round",
            line_width=10,
        )

        legend_it = []
        for grp, df in sub.groupby(renamed_chr_cols[1]):
            data = df[plot_cols].to_dict(orient="list")

            ys = [[seq_dict[i]["plot_name"]] * 2 for i in df[chr_cols[0]]]
            xs = df[x_cols].values.tolist()

            data["xs"] = xs
            data["ys"] = ys

            source = ColumnDataSource(data=data)
            c = p.multi_line(
                xs="xs",
                ys="ys",
                source=source,
                color=cmap[grp],
                line_width=10,
                muted_alpha=0.05,
            )
            legend_it.append((grp, [c]))

        legend_kw = dict(
            spacing=2,
            label_text_font_size="9pt",
            orientation="horizontal",
            click_policy="mute",
        )

        nchr = len(chroms) + 1  # add 1 for unplaced
        nrow = int(np.ceil(nchr / 10) * 10)
        rows = np.split(np.arange(0, nchr), np.arange(0, nrow, 10))[1:]
        legend_it = natsorted(legend_it)
        for ix, row in enumerate(rows):

            if ix == 0:
                title = f"{spps[1]} chromosome (click to mute)"
            else:
                title = ""
            legend = Legend(
                items=legend_it[row[0] : row[-1] + 1],
                title=title,
                **legend_kw,
            )
            p.add_layout(legend, "below")

        p.xaxis.formatter = FuncTickFormatter(
            code="""
            return tick / 1000000 + " Mbp";
        """
        )

        p.yaxis.axis_label = f"{spps[0]} chromosome"

        print(f"Saving plot to {outfile}")
        if show_plot:
            show(p)
        else:
            save(p)
