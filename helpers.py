import pandas as pd

import matplotlib.pyplot as plt

import pybedtools
from io import StringIO


paf_cols = [
    "Query sequence name",
    "Query sequence length",
    "Query start",
    "Query end",
    "Relative strand",
    "Target sequence name",
    "Target sequence length",
    "Target start on original strand",
    "Target end on original strand",
    "Number of residue matches",
    "Alignment block length",
    "Mapping quality",
]


def paf2pandas(paffile:str):
    """parse paf file from minimap to pandas df

    Args:
        paffile (str): Path to paf file to parse.

    Returns:
        pd.DataFrame: _description_
    """    
    paf = pd.read_table(paffile, header=None)

    new_cols = list(paf.columns)
    new_cols[0 : len(paf_cols)] = paf_cols
    paf.columns = new_cols
    sam_tags = paf.apply(
        lambda x: {
            i.split(":")[0]: (":").join(i.split(":")[1:])
            for i in x[len(paf_cols) : paf.shape[1]].to_list()
            if pd.notna(i)
        },
        axis=1,
    )

    paf = paf[paf_cols]
    paf["alignment_tags"] = sam_tags

    return paf


def make_asembly_report(syn_df, top_n: int = 24, query: bool = False):
    """Generate assembly report from PAF file that does not use NCBI assemlies

    Args:
        syn_df (pd.DataFrame): Synteny data frame to process.
        top_n (int, optional): Keep <top_n> largest scaffolds for plotting. Defaults to 24.
        query (bool, optional): Generate report for query (or reference if True). Defaults to False.

    Returns:
        _type_: _description_
    """    
    if query:
        rpt = syn_df.pivot_table(
            index="Query sequence name", values="Query sequence length", aggfunc=max
        )
    else:
        rpt = syn_df.pivot_table(
            index="Target sequence name",
            values="Target sequence length",
            aggfunc=max,
        )
    rpt.columns = ["Sequence-Length"]
    rpt = rpt.sort_values("Sequence-Length", ascending=False)
    rpt["plot_name"] = rpt.index

    seq_dict = {
        row.name: f"{row['plot_name']}" if count < top_n else "grouped"
        for count, (ix, row) in enumerate(rpt.iterrows())
    }

    rpt = rpt.to_dict("index")

    return rpt, seq_dict

def get_coverage_hist(
    paf, query_coverage: bool = False, genomic_only: bool = True
):
    """
    generates summary of coverages for Paf object using bedtools coverage
    """
    syn_df = pd.concat([paf.syn_df_unplaced, paf.syn_df]).reset_index(drop=True)

    ## generate be file for reference from the assumbly report
    ## based on query of reference and genomic only or all scaffolds
    if query_coverage:
        rpt = paf.rpt_qry
        cols = [
            "Query sequence name",
            "Query start",
            "Query end",
        ]
    else:
        rpt = paf.rpt_trgt
        cols = [
            "Target sequence name",
            "Target start on original strand",
            "Target end on original strand",
        ]

    rpt["start"] = 0

    ## generate be file for reference from the assumbly report
    if genomic_only:
        a_bed = rpt.loc[
            rpt["Assigned-Molecule-Location/Type"] == "Chromosome",
            ["start", "Sequence-Length"],
        ]
    else:
        a_bed = rpt[["start", "Sequence-Length"]]

    a = pybedtools.BedTool(StringIO(a_bed.to_csv(sep="\t", header=None)))

    b = pybedtools.BedTool(
        StringIO(
            syn_df[cols]
            .sort_values(cols[0:2])
            .to_csv(sep="\t", header=None, index=False)
        )
    )

    cov = a.coverage(b, hist=True)

    cov = pd.read_table(
        cov.fn,
        header=None,
        names=[
            "feature",
            "start",
            "stop",
            "depth",
            "n_bases",
            "feature_size",
            "fraction",
        ],
    )

    cov_summary = cov.loc[cov["feature"] == "all"]

    cov_summary.columns = [
        "feature",
        "depth",
        "n_bases",
        "feature_size",
        "fraction",
        None,
        None,
    ]
    cov_summary = cov_summary.drop(columns=[None]).reset_index(drop=True)

    cov = cov.loc[cov["feature"] != "all"]

    return cov, cov_summary


def cov_stemplot(cov_summary, title:str=None):
    """
    makes a stem plot using the soverage summary that is 
    returned by get_coverage_hist()
    """
    f = plt.figure()
    markerline, stemlines, baseline = plt.stem(
        cov_summary["depth"],
        cov_summary["fraction"],
        basefmt=" ",
        linefmt="grey",
        markerfmt="go",
    )

    markerline.set_markeredgecolor("black")
    markerline.set_markerfacecolor("orange")
    markerline.set_alpha(0.5)
    plt.xlabel("Depth of Coverage")
    plt.ylabel("Fraction")

    plt.title(title)
    
    return f

